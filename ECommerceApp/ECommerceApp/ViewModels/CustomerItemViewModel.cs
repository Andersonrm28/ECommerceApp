﻿using ECommerceApp.Models;
using ECommerceApp.Services;
using GalaSoft.MvvmLight.Command;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System;

namespace ECommerceApp.ViewModels
{
    public class CustomerItemViewModel : Customer
    {

        #region Attributes
        public DataService dataService;
        public ApiService apiService;
        public NetService netService;
        private NavigationService navigationService;
        #endregion

        #region Properties
        public ObservableCollection<DepartmentItemviewModel> Departments { get; set; }
        public ObservableCollection<CityItemViewModel> Cities { get; set; }
        #endregion

        #region Constructor
        public CustomerItemViewModel()
        {
            //Properties Observables
            Departments = new ObservableCollection<DepartmentItemviewModel>();
            Cities = new ObservableCollection<CityItemViewModel>();

            //Services
            dataService = new DataService();
            apiService = new ApiService();
            netService = new NetService();
            navigationService = new NavigationService();

            //Methods
            LoadDepartments();
            LoadCities();
        }
        #endregion

        #region Commands
        public ICommand CustomerDetailCommand { get { return new RelayCommand(CustomerDetail); } }

        private async void CustomerDetail()
        {
            var customerItemViewModel = new CustomerItemViewModel
            {
                Address = Address,
                City = City,
                CityId = CityId,
                CompanyCustomers = CompanyCustomers,
                CustomerId = CustomerId,
                Department = Department,
                DepartmentId = DepartmentId,
                FirstName = FirstName,
                IsUpdated = IsUpdated,
                LastName = LastName,
                Latitude = Latitude,
                Longitude = Longitude,
                Orders = Orders,
                Phone = Phone,
                Photo = Photo,
                Sales = Sales,
                UserName = UserName,
            };

            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.SetCurrentCustomer(customerItemViewModel);
            await navigationService.Navigate("CustomerDetailPage");
        }
        #endregion

        #region Methods
        private async void LoadDepartments()
        {
            var depratments = new List<Department>();
            if (netService.IsConnected())
            {
                depratments = await apiService.Get<Department>("Departments");
                dataService.Save(depratments);
            }
            else
            {
                depratments = dataService.Get<Department>(true);
            }

            ReloadDepartments(depratments);
        }

        private void ReloadDepartments(List<Department> depratments)
        {
            Departments.Clear();
            foreach (var department in depratments.OrderBy(c => c.Name))
            {
                Departments.Add(new DepartmentItemviewModel
                {
                    Cities = department.Cities,
                    Customers = department.Customers,
                    DepartmentId = department.DepartmentId,
                    Name = department.Name,
                });
            }
        }
        private async void LoadCities()
        {
            var cities = new List<City>();
            if (netService.IsConnected())
            {
                cities = await apiService.Get<City>("Cities");
                dataService.Save(cities);
            }
            else
            {
                cities = dataService.Get<City>(true);
            }

            ReloadCities(cities);
        }

        private void ReloadCities(List<City> cities)
        {
            Cities.Clear();
            foreach (var city in cities.OrderBy(c => c.Name))
            {
                Cities.Add(new CityItemViewModel
                {
                    CityId = city.CityId,
                    Customers = city.Customers,
                    Department = city.Department,
                    DepartmentId = city.DepartmentId,
                    Name = city.Name,
                });
            }
        }
        #endregion
    }
}
