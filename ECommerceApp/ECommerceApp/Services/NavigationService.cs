﻿using ECommerceApp.Pages;
using System.Threading.Tasks;
using System;
using ECommerceApp.Models;
using ECommerceApp.ViewModels;

namespace ECommerceApp.Services
{
    public class NavigationService
    {
        private DataService dataService;

        public NavigationService()
        {
            dataService = new DataService();
        }
        public async Task Navigate(string PageName)
        {
            App.Master.IsPresented = false;

            switch (PageName)
            {
                case "CustomersPage":
                    await App.Navigator.PushAsync(new CustomersPage());
                    break;
                case "DeliveriesPage":
                    await App.Navigator.PushAsync(new DeliveriesPage());
                    break;
                case "OrdersPage":
                    await App.Navigator.PushAsync(new OrdersPage());
                    break;
                case "ProductsPage":
                    await App.Navigator.PushAsync(new ProductsPage());
                    break;
                case "SetupPage":
                    await App.Navigator.PushAsync(new SetupPage());
                    break;
                case "SyncsPage":
                    await App.Navigator.PushAsync(new SyncsPage());
                    break;
                case "UserPage":
                    await App.Navigator.PushAsync(new UserPage());
                    break;
                case "CustomerDetailPage":
                    await App.Navigator.PushAsync(new CustomerDetailPage());
                    break;
                case "NewCustomerPage":
                    await App.Navigator.PushAsync(new NewCustomerPage());
                    break;
                case "LogutPage":
                    Logout();
                    break;
                default:
                    break;
            }
        }

        private void Logout()
        {
            App.CurrentUser.IsRemembered = false;
            dataService.UpdateUser(App.CurrentUser);
            App.Current.MainPage = new LoginPage();
        }

        public void SetMainPage(User user)
        {
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.LoadUser(user);
            App.CurrentUser = user;
            App.Current.MainPage = new MasterPage();
        }

        public User GetCurrentUser()
        {
            return App.CurrentUser;
        }
    }
}
